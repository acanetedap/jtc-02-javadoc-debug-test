package py.com.dz.jtc.javadocDebugTools.racionales;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class PruebaAutomaticaNrosRacionales extends TestCase {
	
	/**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public PruebaAutomaticaNrosRacionales( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( PruebaAutomaticaNrosRacionales.class );
    }

    /**
     * Test cases.. cada funcion de prueba debe comenzar llamandose test (en minusculas)
     */
	public void testSumaOK() throws Exception {
		NumeroRacional a = new NumeroRacional(1,4);
		NumeroRacional b = new NumeroRacional(1,4);
		NumeroRacional expectedResult = new NumeroRacional(8,16);
		
		NumeroRacional result = a.suma(b);
		
		assertEquals(result.getNumerador(), expectedResult.getNumerador());
		assertEquals(result.getDenominador(), expectedResult.getDenominador());		
	}
	
	public void testRestaOK() throws Exception {
		//TODO Pendiente de completar
		assertTrue(true);
	}
	
	public void testMultiplicacionOK() throws Exception {
		//TODO Pendiente de completar
		assertTrue(true);
	}
	
	public void testDivisionOK() throws Exception {
		//TODO Pendiente de completar
		assertTrue(true);
	}
}
