package py.com.dz.jtc.javadocDebugTools.racionales;

/**
 * Clase que representa al error generado y lanzado cuando se intenta crear un NumeroRacional con denominador cero 
 * <br>Curso de Programacion Java
 * @author Derlis Zarate 
 * @see NumeroRacional
 */
public class DenominadorInvalidoException extends Exception {

	/**
	 * Atributo de version para manejo de serializacion
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor por defecto de la clase
	 */
	public DenominadorInvalidoException() {
        super("Denominador Invalido. No se permite cero como denominador");        
    }
    
} //Fin de clase DenominadorInvalidoException
