package py.com.dz.jtc.javadocDebugTools.pila;

public class PruebaPilas {

    public static void main(String[] args) {

        //Creamos una pila de Integers
        Pila<Integer> pila = new Pila<Integer>();
        
        //Creamos otra pila de Strings
        Pila<String> pilaStr = new Pila<String>();
                
        try {
            //cargamos algunos valores int
            pila.apilar(1);
            pila.apilar(2);
            pila.apilar(3);
            
            int n = pila.tamanoPila();
            for (int i=1; i<=n; i++) {
                System.out.println("Valor " + i + ": " + pila.desapilar());
            }                        
            
            //otra forma de iterar..
            pilaStr.apilar("a");
            pilaStr.apilar("b");
            pilaStr.apilar("c");
            while ( ! pilaStr.esVacia()) {
            	System.out.println("Valor : " + pilaStr.desapilar());
            }
                       
        } catch (Exception e) {
            System.out.println("Error al ejecutar el programa. " + e.getMessage());
        }

    } //Fin de main

} //Fin de clase PruebaPilas
