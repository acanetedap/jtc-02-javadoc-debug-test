package py.com.dz.jtc.javadocDebugTools.racionales;

/**
 * Clase que permite probar la ejecucion correcta de numeros racionales de ejemplo
 * <br>Curso de Programacion Java
 * @author Derlis Zarate
 */
public class TestNrosRacionales {

    /**
     * Metodo main de la clase
     * @param args Argumentos de linea de comandos
     */
    public static void main(String[] args) {

        try {
            
            //Creamos 2 nros racionales y aplicamos operaciones sobre ellos
            NumeroRacional x = new NumeroRacional(1, 4); //un cuarto
            NumeroRacional y = new NumeroRacional(1, 2); //un medio

            System.out.printf("\nInicialmente tenemos 2 racionales, x = %s e y = %s", x.toString(), y.toString());

            NumeroRacional res;
            res = x.suma(y);
            System.out.printf("\n\n\tLa suma entre x e y debe ser (6/8) y es %s", res.toString());

            res = x.resta(y);
            System.out.printf("\n\n\tLa resta entre x e y debe ser (-2/8) y es %s", res.toString());

            res = x.producto(y);
            System.out.printf("\n\n\tLa multiplicacion entre x e y debe ser (1/8) y es %s", res.toString());

            res = x.division(y);
            System.out.printf("\n\n\tLa division entre x e y debe ser (2/4) y es %s", res.toString());

            System.out.printf("\n\n\tEl valor real de x = %s es %f", x.toString(), x.getValorReal());

            System.out.printf("\n\n\tEl valor real de y = %s es %f", y.toString(), y.getValorReal());

            System.out.printf("\n\nFin del programa. ");
            
        } catch (DenominadorInvalidoException e) {
            System.err.println(e.getMessage());
        }
        
    } //Fin de main

} //Fin de clase TestNrosRacionales
